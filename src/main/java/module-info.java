module com.vnhp.projectjava1 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens com.vnhp.projectjava1.controller;
    exports com.vnhp.projectjava1.controller;
    
    opens com.vnhp.projectjava1 to javafx.fxml;
    exports com.vnhp.projectjava1;
}
