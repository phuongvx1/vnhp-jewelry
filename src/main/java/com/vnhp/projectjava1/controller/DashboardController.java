/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.projectjava1.controller;

import com.vnhp.projectjava1.App;
import com.vnhp.projectjava1.res.StringValue;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.Modality;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class DashboardController {

    @FXML
    private Button btnDashboard;

    @FXML
    private Button btnProducts;

    @FXML
    private Button btnBills;

    @FXML
    private AnchorPane mainPane;

    @FXML
    public void init() {
        btnDashboard.addEventHandler(ActionEvent.ACTION, eh -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.DASHBOARD_PAGE));
            try {
                AnchorPane dashboard = loader.load();
                mainPane.getChildren().clear();
                mainPane.getChildren().add(dashboard);
                var control = (DashboardController) loader.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnProducts.addEventHandler(ActionEvent.ACTION, eh -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.PRODUCTS_PAGE));
            try {
                AnchorPane products = loader.load();
                mainPane.getChildren().clear();
                mainPane.getChildren().add(products);
                var control = (ProductsController) loader.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });

        btnBills.addEventHandler(ActionEvent.ACTION, eh -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.BILLS_PAGE));
            try {
                AnchorPane bills = loader.load();
                mainPane.getChildren().clear();
                mainPane.getChildren().add(bills);
                var control = (BillsController) loader.getController();
                control.init();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
