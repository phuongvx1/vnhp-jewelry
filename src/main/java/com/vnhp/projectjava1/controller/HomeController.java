/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package com.vnhp.projectjava1.controller;

import com.vnhp.projectjava1.App;
import com.vnhp.projectjava1.res.StringValue;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.event.ActionEvent;
import javafx.event.EventType;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author FairyHunter
 */
public class HomeController {

    String email = "1";
    String password = "1";

    @FXML
    private Button btnLogin;

    @FXML
    private Label lblResult;

    @FXML
    private TextField tfEmail;

    @FXML
    private TextField tfPassword;

    @FXML
    private AnchorPane mainPane;

    @FXML
    public void init() {
        btnLogin.addEventHandler(ActionEvent.ACTION, eh -> {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.DASHBOARD_PAGE));
            String inputEmail = tfEmail.getText();
            String inputPassword = tfPassword.getText();
            if (inputEmail.compareTo(email) == 0 && inputPassword.compareTo(password) == 0) {
                try {
                    AnchorPane dashboard = loader.load();
                    mainPane.getChildren().clear();
                    mainPane.getChildren().add(dashboard);
                    var control = (DashboardController) loader.getController();
                    control.init();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                if (inputEmail.isEmpty() || inputEmail.isBlank() && inputPassword.isEmpty() || inputPassword.isBlank()) {
                    lblResult.setText("Please enter email and password.");
                } else {
                    if (inputEmail.compareTo(email) != 0) {
                        lblResult.setText("Wrong email.");
                    }
                    if (inputPassword.compareTo(password) != 0) {
                        lblResult.setText("Wrong password.");
                    }
                }
            }
        });
    }
}
