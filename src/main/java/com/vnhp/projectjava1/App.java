package com.vnhp.projectjava1;

import com.vnhp.projectjava1.controller.BillsController;
import com.vnhp.projectjava1.controller.DashboardController;
import com.vnhp.projectjava1.controller.HomeController;
import com.vnhp.projectjava1.controller.ProductsController;
import com.vnhp.projectjava1.res.StringValue;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource(StringValue.HOME_PAGE));
        Parent p = loader.load();
        var control = (HomeController) loader.getController();
        control.init();
        scene = new Scene(p, 1000, 600);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}
