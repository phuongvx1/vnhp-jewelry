/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.vnhp.projectjava1.res;

/**
 *
 * @author FairyHunter
 */
public final class StringValue {

    private static String view = "/com/vnhp/projectjava1/view/";
    public static String HOME_PAGE = view + "Home.fxml";
    public static String DASHBOARD_PAGE = view + "Dashboard.fxml";
    public static String PRODUCTS_PAGE = view + "Products.fxml";
    public static String BILLS_PAGE = view + "Bills.fxml";

}
